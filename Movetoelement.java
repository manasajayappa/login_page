package Login_page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Movetoelement 
{

	public static void main(String[] args)
 {
		System.setProperty("webdriver.chrome.driver", "/Users/manasajayappa/Desktop/JARS/Selenium Jars/Drivers/chromedriver");
	    WebDriver driver=new ChromeDriver();
	    driver.get("https://www.amazon.in/");
	    
	    Actions act=new Actions(driver);
	    
	    WebElement category = driver.findElement(By.xpath("//span[contains(text(),'Category')]"));
		
	    WebElement tv= driver.findElement(By.xpath("//span[contains(text(),'TV, Appliances, Electronics')]"));
	    
	    WebElement speakers=driver.findElement(By.xpath("//span[contains(text(),'Speakers')]"));
	    
        act.moveToElement(category)
        .moveToElement(tv)
        .moveToElement(speakers)
        .click()
        .build()
        .perform();

 }

}
