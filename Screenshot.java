package Login_page;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Screenshot
{

	public static void main(String[] args) throws IOException
	{
		System.setProperty("webdriver.chrome.driver", "/Users/manasajayappa/Desktop/JARS/Selenium Jars/Drivers/chromedriver");
		WebDriver driver=new ChromeDriver();
		driver.get("https://www.facebook.com/");
        
		File src=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src,new File("/Users/manasajayappa/Desktop/img.png"));
		
	}

}
