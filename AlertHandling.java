package Login_page;

import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AlertHandling 
{

	public static void main(String[] args) throws InterruptedException 
	{
		 System.setProperty("webdriver.chrome.driver", "/Users/manasajayappa/Desktop/JARS/Selenium Jars/Drivers/chromedriver");
		 WebDriver driver=new ChromeDriver();
		 driver.get("https://ksrtc.in/oprs-web/guest/home.do");
		 
		 driver.findElement(By.id("corover-cb-widget")).click();
		    
		 ////button[@class='btn btn-primary btn-lg btn-block btn-booking']
		 
		 driver.findElement(By.xpath("//button[contains(text(),'Search for Bus')]")).click();
		 Thread.sleep(2000);
         
		 
		/* try
		 {
			 driver.switchTo().alert(); 
		 }
		 catch(NoAlertPresentException e)
		 {
			 e.printStackTrace();
		 }*/	
		 
		 String txt=driver.switchTo().alert().getText();
		 System.out.println(txt);
		 
		 
		 if(txt.matches("Please select Leaving From."))
		 {
			 System.out.println("valid alert");
			 driver.switchTo().alert().accept();
			 
				 
		 }
		 else
		 {
			 System.out.println("invalid alert"); 
		 }
		 driver.close();
	 
		 
	}

}
