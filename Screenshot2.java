package Login_page;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Screenshot2 
{
  static WebDriver driver=null;
  
  public void screen() throws IOException
  {
	  Date d =new Date(0);
	  DateFormat df= new SimpleDateFormat("YYYY-MMM-dd hh mm ss");
	  String value=df.format(d);
	
		
	  File src=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	  FileUtils.copyFile(src,new File("/Users/manasajayappa/Desktop/img"+value+".png"));
		 
  }
  public static void main(String[] args) throws IOException
  {
	 System.setProperty("webdriver.chrome.driver", "/Users/manasajayappa/Desktop/JARS/Selenium Jars/Drivers/chromedriver");
	 WebDriver driver=new ChromeDriver();
	 driver.get("https://www.facebook.com/");
	 Screenshot2 s=new Screenshot2();
	 s.screen();
  }		
}
