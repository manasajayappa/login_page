package Login_page;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Windowhandling {

	public static void main(String[] args) throws InterruptedException 
	{
		System.setProperty("webdriver.chrome.driver", "/Users/manasajayappa/Desktop/JARS/Selenium Jars/Drivers/chromedriver");
		WebDriver driver=new ChromeDriver();
		driver.get("https://www.facebook.com/");
		
		String mainID=driver.getWindowHandle();
		System.out.println(mainID);
		
        driver.findElement(By.id("privacy-link")).click();
        Thread.sleep(2000);
        System.out.println("child-1:"+driver.getTitle());
        
        driver.switchTo().window(mainID) ;
        Thread.sleep(2000);
        
        driver.findElement(By.id("cookie-use-link")).click();
        Thread.sleep(2000);
        System.out.println("child-2:"+driver.getTitle());
        
        Set<String> allWindows=driver.getWindowHandle();
        System.out.println(allWindows.size());
        
        
        for(String x: allWindows)
        {
        	if(!x.equals(mainID))
        	{
        		driver.switchTo().window(x);
        		System.out.println("child-1:"+driver.getTitle());
        	}
        	
        	else
        	{
        	 driver.switchTo().window(mainID);
        	 System.out.println("parent-1:"+driver.getTitle());
        	}
        }
        
        
        
       
         
	}

}
