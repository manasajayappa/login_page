package Login_page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Navigate
{

	public static void main(String[] args) throws InterruptedException 
	{
		System.setProperty("webdriver.chrome.driver", "/Users/manasajayappa/Desktop/JARS/Selenium Jars/Drivers/chromedriver");
        WebDriver driver=new ChromeDriver();
        driver.get("https://www.google.com/");
        
        driver.findElement(By.linkText("Gmail")).click();
        Thread.sleep(1000);
         
        driver.navigate().back();
        Thread.sleep(1000);
        System.out.println("back() called");
        
        
        driver.navigate().forward();
        Thread.sleep(1000);
        System.out.println("forward() called");
        
    
	    driver.navigate().refresh();
	    Thread.sleep(1000);
	    System.out.println("refresh() called");
	
	}
	
	
	
}	
	
	
	
		

	


